﻿
#region Choose Your Own Adventure

/// <summary>
/// Challenge from edabit
/// Check URL for more info
/// URL: https://edabit.com/challenge/HfpK34Ty4SZhN2d9e
/// </summary>
static int License(string me, int agents, string others)
{
    List<String> queueArr = new(others.Split(" "))
    {
        me
    };
    queueArr.Sort();
    // "Adam" "Caroline" "Eric" Frank" "Rebecca"

    int index = queueArr.FindIndex(curEl => curEl.Contains(me));
    int pos_index = index + 1;
    int queueNumber = (pos_index - agents) + 1;

    return queueNumber < 1 ? 20 : queueNumber * 20;
}

//Console.WriteLine(License("Eric", 2, "Adam Caroline Rebecca Frank")); // 40
//Console.WriteLine(License("Zebediah", 1, "Bob Jim Becky Pat")); // 100
//Console.WriteLine(License("Aaron", 3, "Jane Max Olivia Sam")); // 20

#endregion

#region Reverse Coding Challenge #1

/// <summary>
/// Reverse code challenge. Given the input, produce the output.
/// </summary>
static string ReverseCodeOne(string str)
{
    string result = "";
    for (int i = 0; i < str.Length; i++)
    {
        if (Char.IsNumber(str[i]))
        {
            for (int j = 0; j < str[i] - '0'; j++)
            {
                result += str[i - 1];
            }
        }
    }
    return result;
}

//Console.WriteLine(ReverseCodeOne("A4B5C2"));
//Console.WriteLine(ReverseCodeOne("C2F1E5"));
//Console.WriteLine(ReverseCodeOne("T4S2V2"));
//Console.WriteLine(ReverseCodeOne("A1B2C3D4"));

#endregion

#region Reverse Coding Challenge #5

/// <summary>
/// Reverse code challenge. Given the input, produce the output.
/// </summary>
static int ReverseCodeFive(int num)
{
    // Convert num into an array of single digits
    int[] numArray = num.ToString().Select(el => Convert.ToInt32(el) - 48).ToArray();
    // Sort array from Low -> High
    Array.Sort(numArray);
    // Convert array of int to a joined string
    string reverseNum = String.Join("", numArray.Select(el => el.ToString()).ToArray());

    // Calculate Result
    int result = num - Int32.Parse(reverseNum);

    return result < 0 ? 0 : result;
}

//Console.WriteLine(ReverseCodeFive(832));
//Console.WriteLine(ReverseCodeFive(51));
//Console.WriteLine(ReverseCodeFive(7977));
//Console.WriteLine(ReverseCodeFive(1));
//Console.WriteLine(ReverseCodeFive(665));
//Console.WriteLine(ReverseCodeFive(149));

#endregion

#region Power Ranger Coding Challenge

/// <summary>
/// Function that returns the number of positive values raised to the nth power,
/// that lie in the range [a, b], inclusive.
/// </summary>
static int PowerRanger(int n, int a, int b)
{
    int counter = 0;
    for(int i = 1; i <= b; i++)
    {
        int square = (int)Math.Pow(i, n);
        if(square >= a && square <= b)
        {
            counter++;
        }
        if(square > b)
        {
            break;
        }
    }
    return counter;
}

//Console.WriteLine(PowerRanger(2, 49, 65)); //2
//Console.WriteLine(PowerRanger(3, 1, 27)); //3
//Console.WriteLine(PowerRanger(10, 1, 5)); //1
//Console.WriteLine(PowerRanger(5, 31, 33)); //1
//Console.WriteLine(PowerRanger(4, 250, 1300)); //3

#endregion

#region Smooth Sentence Coding Challenge

/// <summary>
/// Function that returns confirms whether a sentence is smooth or not
/// </summary>
static bool IsSmooth(string str)
{
    str = str.ToLower();
    for (int i = 0; i < str.Length; i++)
    {
        if (str[i] == ' ')
        {
            if (str[i - 1] != str[i + 1])
            {
                return false;
            }
        }
    }
    return true;
}

Console.WriteLine(IsSmooth("Marta appreciated deep perpendicular right trapezoids"));
Console.WriteLine(IsSmooth("Someone is outside the doorway"));
Console.WriteLine(IsSmooth("She eats super righteously"));

#endregion